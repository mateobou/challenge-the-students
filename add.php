<?php
	session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="styles.css">
	<title></title>
</head>
<body>
	<?php
		include('header.php');
	?>
	<div>
		<form class="colonne center" action="script.php" method="post">
			<select name="categorie" class="w60x margintb">
				<option name="Design" value="Design">Design</option>
				<option name="Tech" value="Tech">Tech</option>
				<option name="Marketing" value="Marketing">Marketing</option>
			</select>
			<input type="text" name="name" placeholder="Titre" class="">
			<input type="text" name="info" class="paddingb50" placeholder="Description">
			<input type="text" name="temps" placeholder="Temps : (0 pour infini)">

			<!-- À supprimer -->
			<input type="text" name="inspiration" placeholder="Sources d'inspirations (facultatif)">

			<input type="submit" name="add" class="submit br" value="Ajouter">
 		</form>
	</div>
</body>
</html>