<?php
	session_start();
	
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="styles.css">
	<link href="https://fonts.googleapis.com/css?family=Advent+Pro&display=swap" rel="stylesheet"> 
	<script type="text/javascript" src="script.js"></script>
</head>
<body>
	<?php
		include('header.php');
		//echo $_SESSION['name']
	$name = $_SESSION['name']
	?>
	<div class="center">
		<h2 class="txt-center"><?php echo $name;?></h2>
		<h3 class="txt-center">Défis créés</h3>
		<?php
		try
		{
			$bdd = new PDO('mysql:host=localhost;dbname=C2S;charset=utf8', 'root', '');
		}
		catch(Exception $e)
		{
		    die('Erreur : '.$e->getMessage());
		}
		$reponse = $bdd->query('SELECT * FROM challenges');
		$donnees = $reponse->fetchall();
		foreach ($donnees as $challengeInfo) {
			if ($challengeInfo['author_id'] == $_SESSION['id']) {
				$_SESSION['challengeid'] = $challengeInfo['id'];
				echo '
			<div class="line hvr-glow shadow1 margintb txt-center marginrlauto">
				<h2 class="nomargin txt-center">'.$challengeInfo['name'].'</h2>
				<h3 id="'.$challengeInfo['id'].'">'.$challengeInfo['categorie'].'</h3>
				<div class="flex-center">	
					<a href="edit.php?name='.$challengeInfo['name'].'&id='.$challengeInfo['id'].'" class="marginrl10 n hvr-grow">Éditer</a>   <a href="delete.php?name='.$challengeInfo['name'].'&id='.$challengeInfo['id'].'" class="marginrl10 r hvr-grow">Supprimer</a>
				</div>
				
			</div>	
			';
			if ($challengeInfo['categorie'] == 'Design') {
					?>
						<script type="text/javascript">
							var newlenght = Design.push(<?php echo $challengeInfo['id']; ?>);
							console.log('Design : ',Design); 
						</script>

					<?php
					# code...
				}
				elseif ($challengeInfo['categorie'] == 'Tech') {
					?>
						<script type="text/javascript">
							var newlenght = Tech.push(<?php echo $challengeInfo['id']; ?>);
							console.log('Tech : ',Tech); 
						</script>

					<?php
					# code...
				}
				elseif ($challengeInfo['categorie'] == 'Marketing') {
					?>
						<script type="text/javascript">
							var newlenght = Marketing.push(<?php echo $challengeInfo['id']; ?>);
							console.log('Marketing : ', Marketing); 
						</script>

					<?php
					# code...
				}
			}

		}
		?>
		<script type="text/javascript">
			ChangeColorExec();
		</script>

		<h3 class="txt-center">Défis favoris</h3>
	</div>
</body>
</html>